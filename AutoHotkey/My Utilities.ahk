; # AutoRun

	;= Init
		#NoEnv
		SendMode Input
		;Ensure consistent starting directory
		SetWorkingDir %A_ScriptDir%
		 ; Ensures that there is only a single instance of this script running.
		#SingleInstance, Force

    ;= Load Settings
    settingsFileName = \My_Utilities.ini
    global settingsINIPath := A_scriptdir . settingsFileName
    IniRead, myMicId, %settingsINIPath%, MicrophoneID, Key1 ;myMicId - Microphone ID to mute

; Variables for storing values globally, do not modify them
global wowmessage = ""
; ### Hotstrings

; #### Per App Hotstrings

; ## AutoRan Functions
SetLockKeyStates()
AppGUICreate()

; # AutoRun End
Return

; ## Settings

; ### Hotkeys
#UseHook

Tab & MButton::
MButton & Tab::
Pause & ScrollLock::
ScrollLock & Pause::ToggleMic()

^SPACE:: Winset, Alwaysontop, , A ; Pins a window
;  ====Fixes===
$MButton::MButton
Pause:: Send {Pause}
ScrollLock::ScrollLock
Tab::Tab
; #### Per App Hotkeys
;===================Save Reload group===================
    #IfWinActive, ahk_group saveReload
    ~^s:: SaveReload() ;reloads the scripts
    return
    ^Esc:: ExitApp ;closes the script
		#IfWinActive
;==================================================
;===================League Of Legends===================
#IfWinActive ahk_exe LeagueClientUx.exe
    Numpad4:: TypeLeagueRole("Top")
    return
    Numpad1:: TypeLeagueRole("Jungle")
    return
    Numpad2:: TypeLeagueRole("Mid")
    return
    Numpad6:: TypeLeagueRole("ADC")
    return
    Numpad3:: TypeLeagueRole("Supp")
    return
    #IfWinActive
;==================================================
;=====================WOW Classic=====================
 #IfWinActive ahk_exe WowClassic.exe
    ^+D:: SendWOWMessage()
    ^+!D:: SetWOWMessage()
	#IfWinActive
;==================================================
; # Functions
SetLockKeyStates() {
        SetNumlockState, AlwaysOn
        ;SetCapsLockState, AlwaysOff
    }

varExist(ByRef v) { ; Requires 1.0.46+
   return &v = &n ? 0 : v = "" ? 2 : 1
}

random( x, y ) {
    Random, var, %x%, %y%
    return var
    }

 SendWOWMessage() {
        Items := Array("Wanna go", "Would you like to go", "Interested in doing", "Wanna do")
        If (wowmessage = "") {
            MsgBox Dungeon is not set.
            return
        }
        text := Items[random(1, Items.MaxIndex() )]
        SendInput, %text% %wowmessage%?
        Send {Enter}
        return
    }

    SetWOWMessage() {
        InputBox, UserInput, Enter dung name, Please enter dung name:, , 300, 150
        wowmessage = %UserInput%
        return
    }

	TypeLeagueRole(role) {
        SendInput, %role%
        Send {Enter}
        return
    }

	ToggleMic() {
        global myMicId

        if (myMicId = "ERROR") {
            MsgBox, Microphone ID is not set.
            return
        }

        SoundSet, +1, MASTER, mute, %myMicId%
        SoundGet, master_mute, , mute, %myMicId%
        if (master_mute = "on")
        {
            ToolTip, Mic muted
            SoundPlay, %A_ScriptDir%\My_Utilities_assets\MicMuteToggle_mute.wav
        }
        else
        {
            Tooltip, Mic listening
            SoundPlay, %A_ScriptDir%\My_Utilities_assets\MicMuteToggle_unmute.wav
        }
        SetTimer, RemoveToolTip, 500
        return
    }

	RemoveTrayTip() {
        SetTimer, RemoveTrayTip, Off
        TrayTip
    }

    RemoveTooltip() {
        SetTimer, RemoveToolTip, Off
        ToolTip
    }

	 SaveReload() {
        TrayTip, Reloading updated script, %A_ScriptName%
        SetTimer, RemoveTrayTip, 1500
        Sleep, 1750
        Reload
        return
    }

    ; ## Gui Functions

    AppGUICreate() {
        global myMicId

        global AppGui_1_myMicId=

        Gui, Add, Tab3,, Microphone ;|Other

        Gui, Tab, 1 ; GUI  Mic Tab
        Gui, Add, Text, , Mic ID (For muting)
        Gui, Add, Edit, vAppGui_1_myMicId, %myMicId%
        Gui, Add, Button, gAppGui_1_Submit, Done

        ; Add it to Menu Tray
        Menu, Tray, Add, Settings, AppGUIShow
    }
    AppGUIShow() {
        global myMicId

        GuiControl,, AGUI_1_myMicId, %myMicID%
        Gui, Show
    }

    AppGui_1_Submit() { ; GUI Mic Tab Submit
        global myMicId
        global settingsINIPath

        Gui, Submit
        GuiControlGet, myMicId_new,, AppGui_1_myMicId
        myMicId = %myMicId_new%
        IniWrite, %myMicId_new%, %settingsINIPath%, MicrophoneID, Key1
    }
; # Classes